# Ruby Test Runner

![Icon](images/icon.png)

Run Ruby tests with one click (or key binding)!

This extension is mainly targeted at Ruby on Rails projects. Though it is highly customizable through extension settings, where you may provide custom commands for running different kinds of tests.

Update: Live test results in the editor!
![Live Test Results](images/live-tests-overview.gif)

## Features

It is possible to run Ruby tests with one click.

![Overview](images/overview.gif)

This extension makes use of CodeLens to let you run single tests within a test file with just a click.

![CodeLens](images/code-lens.png)

There are three Status Bar Buttons:
* Unit tests - runs all unit tests
* System tests - runs all system tests
* Run test - runs the currently open test file. When there is a selection in the test file, the starting line of the selection will be passed to the test runner
  
![Buttons](images/buttons.png)

There is a key binding for running the currently open test file with `Ctrl + Shift + T`. It works similarily to the 'Run test' Status Bar Button. When there is a selection of text in the current test file, the starting line of this selection will be passed to the test runner after a semicolon. (eg. `rails t /Users/some_user/some_test.rb:27`)

![Key bindings](images/keybindings.gif)

Another keybinding is `Ctrl + Shift + a` which runs all unit tests.

## Extension Settings

| Setting name | Description | Default value |
| ------------ | ----------- | ------------- |
| `rubyTestRunner.unitTestCommand` | Provide a custom command for running unit tests | `"rails t"` |
| `rubyTestRunner.unitTestCommandRails4` | Provide a custom command for running unit tests in Rails < 5 | `"rake test TEST="` |
| `rubyTestRunner.specTestCommand` | Provide a custom command for running spec tests | `"rspec"` |
| `rubyTestRunner.systemTestCommand` | Provide a custom command for running system tests | `"rails test:system"` |
| `rubyTestRunner.systemTestCommandWithoutRails` | Provide a custom command for running system tests in projects without Rails system tests | `"rspec"` |
| `rubyTestRunner.recognizedTestMethodNames` | Method names which the extension will recognize as test definitions, seperated with a comma | `"should, scenario, context, describe, it, test"` |
| `rubyTestRunner.runTestCodeLensEnabled` | Choose whether the dynamic 'Run test' code lenses in the source code view shall be generated | `true` |
| `rubyTestRunner.runAllUnitTestsButtonEnabled` | Choose whether the 'Unit tests' Status Bar Button should be shown | `true` |
| `rubyTestRunner.runAllSystemTestsButtonEnabled` | Choose whether the 'System tests' Status Bar Button should be shown | `true` |
| `rubyTestRunner.runTestButtonEnabled` | Choose whether the 'Run test file' Status Bar Button should be shown | `false` |
| `rubyTestRunner.openTestsInIterm` | Open tests in iTerm instead of the VSCode terminal | `false` |

## Keybindings

You can edit the keybindings by pasting this in Code -> Preferences -> Keyboard Shortcuts -> keybindings.json:

```json
{
  "key": "ctrl+shit+t",
  "command": "ruby-test-runner.run-current-test-file",
  "when": "editorTextFocus"
},
{
  "key": "ctrl+shift+a",
  "command": "ruby-test-runner.run-all-unit-tests",
  "when": "editorTextFocus"
}
```

## Release Notes

### 0.3.5

Slight refactoring and fixes for live test results - old test results are now properly deleted when a new test run is initialized

### 0.3.4

The extension will now automatically change the current working directory of the integrated terminal when accessing local gems in the `lib` directory

### 0.3.3

Errors in minitests are now properly registered an displayed in live test results

### 0.3.2

Failures outside of test definitions are now properly handled in live test results

### 0.3.1

Experimental feature - Live test result feed in the editor added

### 0.2.7

Running single tests now works properly in Rails 4

### 0.2.6

AppleScript used to run tests in iTerm replaced by a JXA (JavaScript for automation) script

### 0.2.5

It's now possible to run tests in iTerm instead of the integrated VSCode terminal.

Added new settings:
* `rubyTestRunner.openTestsInIterm`

### 0.2.4

Small naming fixes.

### 0.2.3

Added a new `Run current test file` button at the top of the editor.

### 0.2.2

A small fix to make the extension functional when no workspace is present.

### 0.2.1

The extension will now recognise the Rails version of your open project and use appropriate commands. Same goes for system tests, the extension will look for the `test/application_system_test_case.rb` file to see whether to use the `rails test:system` or the `rspec` command.

Added new settings:
* `rubyTestRunner.systemTestCommandWithoutRails`
* `rubyTestRunner.unitTestCommandRails4`

### 0.2.0

Added new settings:
* `rubyTestRunner.recognizedTestMethodNames`
* `rubyTestRunner.runTestCodeLensEnabled`
* `rubyTestRunner.runAllUnitTestsButtonEnabled`
* `rubyTestRunner.runAllSystemTestsButtonEnabled`
* `rubyTestRunner.runTestButtonEnabled`

### 0.1.4

Added CodeLens commands to run single tests within a test file with just a click.

### 0.1.3

Changed keybindings to use `Ctrl` instead of `Cmd` (buggy).

### 0.1.2

Added `Cmd + Shift + a` keybinding to run all unit tests.
Additional description of how to change keybindings in README.

### 0.1.1

Better README and icon added.

### 0.1.0

Initial release. It is possible to run Ruby tests with one click. There are three Status Bar Buttons:
* Unit tests - runs all unit tests
* System tests - runs all system tests
* Run test - runs the currently open test file. When there is a selection in the test file, the starting line of the selection will be passed to the test runner after a semicolon. (eg. `rails t /Users/some_user/some_test.rb:27`)

There is a key binding for running the currently open test file with `Shift + Cmd + T` or `Shift + Ctrl + T`