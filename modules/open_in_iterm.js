#!/usr/bin/env osascript -l JavaScript

// JXA automation script!

function run(args) {
  const iterm = Application('iTerm')
  iterm.includeStandardAdditions = true
  iterm.activate()

  let window = iterm.currentWindow()

  if(!window) window = iterm.createWindowWithDefaultProfile()

  let tab = window.tabs[0]
  if(!tab) tab = window.createTabWithDefaultProfile()
  let session = tab.currentSession()

  window.revealHotkeyWindow()
  tab.select()

  if(args) session.write({ text: args.join(' ') })
}
