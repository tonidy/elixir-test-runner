const vscode = require('vscode')
const path = require('path')
const { spawnSync } = require('child_process')

const { VscodeTerminalAdapter } = require("./vscode_terminal_adapter.js")
const { ItermTerminalAdapter } = require("./iterm_terminal_adapter.js")
const { RailsSixCommandGenerator } = require("./rails_six_command_generator.js")
const { RailsFourCommandGenerator } = require("./rails_four_command_generator.js")
const { RunTestCodeLensProvider } = require("./run_test_code_lens_provider.js")
const { TestOutputInterpeter } = require("./test_output_interpreter.js")
const { TestStateDecorator } = require("./test_state_decorator.js")

class RubyTestRunner {
                          // `rails (6.11.3rc2)` or ` rails(4.2.1)` etc
  railsGemfileLockRegex = /^\s*rails\s*\((\d+\.)+(\d|\w)+\)\s*$/

  unitTestErrorStartRegex = /ERROR|Error|FAIL|Failure/

  rspecErrorStartRegex = /^(Failure\/Error:|Failure:|Error:)/
  rspecHaltInterpretingRegex = /^\s*Failed examples:{0,1}/


  runCurrentTestFileIconId = 'ruby-test-runner.run-current-test-file-icon'
  runCurrentTestFileId = 'ruby-test-runner.run-current-test-file'
  runAllUnitTestsId = 'ruby-test-runner.run-all-unit-tests'
  runAllSystemTestsId = 'ruby-test-runner.run-all-system-tests'
  runUnitTestId = 'ruby-test-runner.run-unit-test'
  runRspecId = 'ruby-test-runner.run-rspec'
  runTestCodeLensId = 'ruby-test-runner.run-test-code-lens'

  constructor(context) {
    this.context = context
    this.railsPrimaryVersion = 6
    this.config = vscode.workspace.getConfiguration('rubyTestRunner')
    this.testMethodNames = this.config.recognizedTestMethodNames.split(',').map((el) => { return el.trim() }).join('|')
    this.testRegex = new RegExp(`(${this.testMethodNames})\\s+("|').*("|')\\s+do`)
    this.railsSystemTestsPresent = false
    this.testsResults = {}
  }

  activate() {
    console.log('Ruby Test Runner activated')

    this.parseRailsVersion()
  }

  continueActivation() {
    if(!this.gemfileScanCompleted || !this.systemTestsScanCompleted) return

    let rubyTestRunner = this

    this.testStateDecorator = new TestStateDecorator(this.context)

    let printAndSavePath
    if(this.liveTestResultsEnabled()) {
      printAndSavePath = path.join(__dirname, '..', 'print_and_save')
      let result = spawnSync('chmod', ['+x', printAndSavePath])
      console.log(result.stderr.toString())

      this.context.subscriptions.push(vscode.window.onDidChangeActiveTextEditor((editor) => { rubyTestRunner.displayTestResults(editor) }))
      this.testOutputPath = path.join(__dirname, '..', 'tests.out')
      this.testOutputInterpreter = {
        ["unit-test"]: new TestOutputInterpeter(this, this.testOutputPath, 3, this.unitTestErrorStartRegex),
        ["rspec"]: new TestOutputInterpeter(this, this.testOutputPath, 3, this.rspecErrorStartRegex, this.rspecHaltInterpretingRegex),
      }
    }

    if(this.config.runTestCodeLensEnabled) {
      this.codeLensProvider = new RunTestCodeLensProvider(this.testRegex, this.methodNamesRegex)
      this.context.subscriptions.push(vscode.languages.registerCodeLensProvider('ruby', this.codeLensProvider))
    }

    if(process.platform === "darwin" && this.config.openTestsInIterm) {
      this.terminalAdapter = new ItermTerminalAdapter(this.workspaceRoot)
    } else {
      this.terminalAdapter = new VscodeTerminalAdapter()
    }

    let systemTestCommand = this.railsSystemTestsPresent ? this.config.systemTestCommand : this.config.systemTestCommandWithoutRails

    if(this.railsPrimaryVersion < 5) {
      this.commandGenerator = new RailsFourCommandGenerator(this.config.unitTestCommandRails4, systemTestCommand, this.config.specCommand, printAndSavePath)
    } else {
      this.commandGenerator = new RailsSixCommandGenerator(this.config.unitTestCommand, systemTestCommand, this.config.specCommand, printAndSavePath)
    }

    this.registerCommands()
    this.registerButtons()
    if(this.terminalAdapter.vscode()) this.context.subscriptions.push(vscode.window.onDidChangeActiveTextEditor((editor) => { rubyTestRunner.updateTerminalWorkingDirectory(editor) }))
  }

  unixPlatform() {
    switch(process.platform) {
      case 'darwin': 
      case 'freebsd': 
      case 'linux': 
        return true
    }

    return false
  }

  windowsPlatform() {
    if(process.platform == 'win32') return true

    return false
  }

  liveTestResultsEnabled() {
    return this.unixPlatform() && this.config.liveTestResultsEnabled
  }

  parseRailsVersion() {
    if(!vscode.workspace.workspaceFolders || !vscode.workspace.workspaceFolders[0]) return
  
    this.workspaceRoot = vscode.workspace.workspaceFolders[0].uri
    this.currentWorkingDirectory = this.workspaceRoot

    let rubyTestRunner = this
  
    // scan Gemfile.lock to get Rails version
    vscode.workspace.openTextDocument(vscode.Uri.joinPath(this.workspaceRoot, 'Gemfile.lock')).then((document) => {
      for (let i = 0; i < document.lineCount; i++) {
        let line = document.lineAt(i)
        let trimmedLine = line.text.trim().substr(0, 150)
        let regexMatch = rubyTestRunner.railsGemfileLockRegex.exec(trimmedLine)
    
        if(regexMatch) {
          console.log(regexMatch[0])
          // `rails (6.11.3rc2)` => 6
          // `rails (4.1.2)` => 4
          rubyTestRunner.railsPrimaryVersion = parseInt(regexMatch[0].replace('rails', '').replace('(', '').trim()[0])
        }
  
        if(rubyTestRunner.railsPrimaryVersion && rubyTestRunner.railsPrimaryVersion < 5) rubyTestRunner.methodNamesRegex = new RegExp(`${rubyTestRunner.testMethodNames}|do|"|'`, 'g')
      }

      rubyTestRunner.gemfileScanCompleted = true
      rubyTestRunner.continueActivation()
    }, (reason) => {
      rubyTestRunner.gemfileScanCompleted = true
      rubyTestRunner.continueActivation()
    })
  
    vscode.workspace.openTextDocument(vscode.Uri.joinPath(this.workspaceRoot, 'test/application_system_test_case.rb')).then((document) => {
      rubyTestRunner.railsSystemTestsPresent = true

      rubyTestRunner.systemTestsScanCompleted = true
      rubyTestRunner.continueActivation()
    }, (reason) => {
      rubyTestRunner.systemTestsScanCompleted = true
      rubyTestRunner.continueActivation()
    })
  }

  allButtonsDisabled() {
    return !this.config.runAllUnitTestsButtonEnabled && !this.config.runAllSystemTestsButtonEnabled && !this.config.runTestButtonEnabled
  }

  updateRunTestBtns(editor = vscode.window.activeTextEditor) {
    if(!editor || !editor.document || !editor.document.fileName || editor.document.languageId !== 'ruby') {
      if(this.config.runTestButtonEnabled) this.runTestButton.hide()
      if(!this.workspaceRoot) return
  
      if(this.config.runAllUnitTestsButtonEnabled) this.runAllUnitTestsButton.hide()
      if(this.config.runAllSystemTestsButtonEnabled) this.runAllSystemTestsButton.hide()
    } else {
      if(this.config.runTestButtonEnabled) this.runTestButton.hide()
  
      if(this.workspaceRoot) {
        if(this.config.runAllUnitTestsButtonEnabled) this.runAllUnitTestsButton.show()
        if(this.config.runAllSystemTestsButtonEnabled) this.runAllSystemTestsButton.show()
      }
      
      if(!this.config.runTestButtonEnabled) return
  
      switch(editor.document.fileName.split('_').slice(-1)[0]) {
        case 'spec.rb':
          this.runTestButton.command = this.runRspecId
          this.runTestButton.text = `$(testing-run-icon) Run spec file`
          this.runTestButton.show()
          break
        case 'test.rb':
          this.runTestButton.command = this.runUnitTestId
          this.runTestButton.text = `$(testing-run-icon) Run test file`
          this.runTestButton.show()
          break
      }
    }
  }

  updateTerminalWorkingDirectory(editor = vscode.window.activeTextEditor) {
    if(!editor || !editor.document || !editor.document.fileName || editor.document.languageId !== 'ruby') return

    let newWorkingDirectory = this.workspaceRoot.fsPath
    let separator = path.sep
    let pathArray = editor.document.fileName.split(`${separator}lib${separator}`)

    if(pathArray.length > 1) {
      newWorkingDirectory = path.join(newWorkingDirectory, 'lib', pathArray[1].split(separator)[0])
    }

    if(newWorkingDirectory != this.currentWorkingDirectory) {
      let clearCommand = this.windowsPlatform() ? 'cls' : 'clear'
      this.currentWorkingDirectory = newWorkingDirectory
      this.terminalAdapter.run(`cd ${newWorkingDirectory} && ${clearCommand}`, false)
    }
  }

  registerCommands() {
    let rubyTestRunner = this

    this.context.subscriptions.push(vscode.commands.registerCommand(this.runCurrentTestFileIconId, () => { rubyTestRunner.runCurrentTestFile() }))
    this.context.subscriptions.push(vscode.commands.registerCommand(this.runCurrentTestFileId, () => { rubyTestRunner.runCurrentTestFile() }))
    this.context.subscriptions.push(vscode.commands.registerCommand(this.runTestCodeLensId, (match) => { rubyTestRunner.runTestCodeLens(match) }))
    this.context.subscriptions.push(vscode.commands.registerCommand(this.runUnitTestId, function () {
      rubyTestRunner.handleCurrentFileTestRun('unit-test')
    }))
    this.context.subscriptions.push(vscode.commands.registerCommand(this.runRspecId, function () {
      rubyTestRunner.handleCurrentFileTestRun('rspec')
    }))

    if(vscode.workspace.workspaceFolders) {
      this.context.subscriptions.push(vscode.commands.registerCommand(this.runAllUnitTestsId, function () {
        rubyTestRunner.handleCurrentFileTestRun('all-unit-tests')
      }))
    
      this.context.subscriptions.push(vscode.commands.registerCommand(this.runAllSystemTestsId, function () {
        rubyTestRunner.handleCurrentFileTestRun('all-system-tests')
      }))
    }
  }

  registerButtons() {
    let rubyTestRunner = this

    if(vscode.workspace.workspaceFolders) {
      if(this.config.runAllUnitTestsButtonEnabled) {
        let runAllUnitTestsButton = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 3)
        runAllUnitTestsButton.command = this.runAllUnitTestsId
        runAllUnitTestsButton.text = `$(testing-run-all-icon) Unit tests`
        this.context.subscriptions.push(runAllUnitTestsButton)
        this.runAllUnitTestsButton = runAllUnitTestsButton
      }
    
      if(this.config.runAllSystemTestsButtonEnabled) {
        let runAllSystemTestsButton = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 2)
        runAllSystemTestsButton.command = this.runAllSystemTestsId
        runAllSystemTestsButton.text = `$(testing-run-all-icon) System tests`
        this.context.subscriptions.push(runAllSystemTestsButton)
        this.runAllSystemTestsButton = runAllSystemTestsButton
      }
    }

    if(this.config.runTestButtonEnabled) {
      let runTestButton = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 1)
      this.context.subscriptions.push(runTestButton)
      this.runTestButton = runTestButton
    }

    if(!this.allButtonsDisabled()) {
      this.context.subscriptions.push(vscode.window.onDidChangeActiveTextEditor((editor) => { rubyTestRunner.updateRunTestBtns(editor) }))
      this.updateRunTestBtns()
    }
  }

  handleCurrentFileTestRun(type = 'unit-test') {
    let editor = vscode.window.activeTextEditor
    if(!editor || !editor.document) return
  
    let command = this.commandGenerator.getTestCommand(editor.document.fileName, type)
    this.terminalAdapter.run(command)

    if(!this.liveTestResultsEnabled()) return

    let rubyTestRunner = this
    this.removeFromTestResults(editor.document.fileName)
    this.testStateDecorator.clearDecorations(editor)

    let result = spawnSync('rm', [this.testOutputPath])
    console.log(result.stderr.toString())

    setTimeout(() => {
      rubyTestRunner.checkTestOutput(editor, type)
    }, 5000)
  }

  checkTestOutput(editor, type, targetedTestObject = null, readAttempt = 0) {
    // break the cycle after 200 attempts (10 minutes)
    if(readAttempt > 200) return

    let rubyTestRunner = this
    let fileName = editor.document.fileName

    let result = spawnSync('ls', [rubyTestRunner.testOutputPath])
    if(result.stderr.toString().length > 1) {
      setTimeout(() => {
        rubyTestRunner.checkTestOutput(editor, type, targetedTestObject, readAttempt + 1)
      }, 3000)
      return
    }

    let testOutputInterpeter = this.testOutputInterpreter[type]
    testOutputInterpeter.getFailedTests(fileName.replace(this.workspaceRoot.fsPath, '').replace(/\/|\\/, '')).then((result) => {
      let failedTests = result[0]
      let failedTestsErrorLines = result[1]
      let passedTests = []

      passedTests = testOutputInterpeter.getPassedTests(failedTests, targetedTestObject)

      rubyTestRunner.addToTestResults(fileName, {
        passedTests: passedTests,
        failedTests: failedTests,
        failedTestsErrorLines: failedTestsErrorLines
      })

      let activeEditor = vscode.window.activeTextEditor
      if(activeEditor.document && activeEditor.document.fileName == fileName) {
        rubyTestRunner.displayTestResults(activeEditor)
      }
    }, (reason) => {})
  }

  displayTestResults(editor) {
    if(!editor || !editor.document) return

    let fileName = editor.document.fileName
    let currentTestResults = this.testsResults[fileName]
    if(!currentTestResults) return

    this.testStateDecorator.decoratePassedGutter(editor, currentTestResults.passedTests)
    this.testStateDecorator.decorateFailedGutter(editor, currentTestResults.failedTests)
    this.testStateDecorator.decorateErrorLine(editor, currentTestResults.failedTestsErrorLines)
  }

  addToTestResults(fileName, testResults) {
    this.testsResults[fileName] = testResults

    let keys = Object.keys(this.testsResults)
    if(keys > 3) delete this.testsResults[keys.shift()]
  }

  removeFromTestResults(fileName) {
    if(!this.testsResults[fileName]) return

    delete this.testsResults[fileName]
  }
  
  runTestCodeLens(match) {
    let editor = vscode.window.activeTextEditor
    let command = this.commandGenerator.getTestCommand(match.fileName, match.type, match.lineNumber, match.testName)
    this.terminalAdapter.run(command)

    if(!this.liveTestResultsEnabled()) return

    let rubyTestRunner = this
    this.removeFromTestResults(editor.document.fileName)
    this.testStateDecorator.clearDecorations(editor)

    let result = spawnSync('rm', [this.testOutputPath])
    console.log(result.stderr.toString())

    setTimeout(() => {
      rubyTestRunner.checkTestOutput(editor, match.type, match)
    }, 5000)
  }
  
  runCurrentTestFile() {
    let editor = vscode.window.activeTextEditor
    if(!editor || !editor.document || !editor.document.fileName || editor.document.languageId !== 'ruby') return
  
    switch(editor.document.fileName.split('_').slice(-1)[0]) {
      case 'spec.rb':
        this.handleCurrentFileTestRun('rspec')
        return
      case 'test.rb':
        this.handleCurrentFileTestRun('unit-test')
        return
    }
  }
}

module.exports = {
  RubyTestRunner
}