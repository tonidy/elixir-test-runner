const vscode = require('vscode')

class RunTestCodeLensProvider {
  // `  end # some comment ` or `  end `
  endRegex = /^\s*end\s*(#.*)*$/
  definitionsEndingWithEnd = /^\s*(([\w\ ,.()'"]*\s+do)|((if|unless|while|for|begin|case|def)[^\n\r{}]*))$/

  constructor(testOrGroupDefinitionRegex, methodNamesRegex = null) {
    this.testOrGroupDefinitionRegex = testOrGroupDefinitionRegex
    this.methodNamesRegex = methodNamesRegex
    this.testStructures = []
  }

  provideCodeLenses(document, token) {
    switch(document.fileName.split('_').slice(-1)[0]) {
      case 'spec.rb':
        return this.provideCodeLensesSpec(document, token)
      case 'test.rb':
        return this.provideCodeLensesUnit(document, token)
    }
  
    return []
  }

  provideCodeLensesUnit(document, token) {
    const matches = this.findTestDefinitions(document, 'unit-test')
    return matches.map(match => new vscode.CodeLens(match.range, {
        title: '$(testing-run-icon) Run test',
        command: 'ruby-test-runner.run-test-code-lens',
        arguments: [match]
    }))
  }
  
  provideCodeLensesSpec(document, token) {
    const matches = this.findTestDefinitions(document, 'rspec')
    return matches.map(match => new vscode.CodeLens(match.range, {
        title: '$(testing-run-icon) Run test',
        command: 'ruby-test-runner.run-test-code-lens',
        arguments: [match]
    }))
  }

  findTestDefinitions(document, type) {
    let matches = []
    let currentTestParent = null
    let currentParentIsATest = null
    let testObject, line, trimmedLine, testOrGroupDefinition
    let testObjectStack = []
  
    this.clearTestStructures()
    for (let i = 0; i < document.lineCount; i++) {
      line = document.lineAt(i)
      trimmedLine = line.text.trim().substr(0, 400)
      testOrGroupDefinition = trimmedLine.match(this.testOrGroupDefinitionRegex)

      if(testOrGroupDefinition) {
        testObject = {
          range: new vscode.Range(new vscode.Position(i, 1), new vscode.Position(i, trimmedLine.length - 1)),
          lineNumber: i + 1,
          fileName: document.fileName,
          type: type,
          children: [],
          parent: null
        }

        if(this.methodNamesRegex) testObject.testName = trimmedLine.replace(this.methodNamesRegex, '').trim().replace(/\ /g, '_')

        if(currentTestParent) {
          testObject.parent = currentTestParent
          currentTestParent.children.push(testObject)
        } else {
          this.testStructures.push(testObject)
        }

        currentTestParent = testObject
        testObjectStack.push(true)
  
        matches.push(testObject)
      } else {
        if(this.definitionsEndingWithEnd.exec(trimmedLine)) {
          testObjectStack.push(false)
          continue
        }

        if(!this.endRegex.exec(trimmedLine) || !currentTestParent) continue

        currentParentIsATest = testObjectStack.pop()
        if(currentParentIsATest) currentTestParent = currentTestParent.parent
      }
    }
  
    return matches
  }

  clearTestStructures() {
    this.testStructures = []
  }
}

module.exports = {
  RunTestCodeLensProvider
}