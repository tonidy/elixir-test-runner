const vscode = require('vscode')
const path = require('path')

class TestStateDecorator {
  constructor(context) {
    this.passedGutter = vscode.window.createTextEditorDecorationType({ gutterIconPath: path.join(__dirname, '..', 'resources', 'passed.svg'), overviewRulerColor: '#27dc11ba', overviewRulerLane: vscode.OverviewRulerLane.Center })
    this.failedGutter = vscode.window.createTextEditorDecorationType({ gutterIconPath: path.join(__dirname, '..', 'resources', 'failed.svg'), overviewRulerColor: '#dc1137ba', overviewRulerLane: vscode.OverviewRulerLane.Center })
    this.errorLine = vscode.window.createTextEditorDecorationType({ backgroundColor: '#dc113766', overviewRulerColor: '#dc113766', overviewRulerLane: vscode.OverviewRulerLane.Right })
    
    context.subscriptions.push(this.passedGutter)
    context.subscriptions.push(this.failedGutter)
    context.subscriptions.push(this.errorLine)
  }

  decoratePassedGutter(editor, options) {
    editor.setDecorations(this.passedGutter, options)
  }

  decorateFailedGutter(editor, options) {
    editor.setDecorations(this.failedGutter, options)
  }

  decorateErrorLine(editor, options) {
    editor.setDecorations(this.errorLine, options)
  }

  clearDecorations(editor) {
    editor.setDecorations(this.passedGutter, [])
    editor.setDecorations(this.failedGutter, [])
    editor.setDecorations(this.errorLine, [])
  }
}

module.exports = {
  TestStateDecorator
}